# `prll`

Allows to run bash commands in parallel, like:
```bash
find . -type f -name "*.png" | while read f
do
  printf 'zopflipng -y %1$q %1$q\n' "$f"
done | prll 12
```
* How to build on Mac:
```bash
clang -O2 prll.c -o prll
```