#define _XOPEN_SOURCE 700

#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

extern char **environ;

unsigned maxjobs, njobs;
unsigned nerrors, nsucceses;

int free_job;
pid_t *jobs;
char **new_env;
#define ENV_SIZE 64

void wait_for_child()
{
	pid_t pid;
	int status;
	int i;

	for (;;) {
		pid = wait(&status);
		if (pid < 0) {
			perror("wait");
			exit(1);
		}
		if (WIFEXITED(status)) {
			if (WEXITSTATUS(status)) {
				fprintf(stderr, "child %d exited with %d\n",
				                pid,
				                WEXITSTATUS(status));
				nerrors++;
			} else
				nsucceses++;
		} else if (WIFSIGNALED(status)) {
			fprintf(stderr, "child %d terminated by signal %d\n",
					pid,
					WTERMSIG(status));
			nerrors++;
		}
		njobs--;
		for (i = 0; i < maxjobs; i++)
			if (jobs[i] == pid) {
				free_job = i;
				break;
			}
		return;
	}
}

pid_t runcmd(char *cmd, int th, int n)
{
	pid_t pid;

	pid = fork();
	if (pid < 0) {
		perror("fork");
		nerrors++;
		return -1;
	}
	if (!pid) {
		close(0);
		snprintf(new_env[0], ENV_SIZE, "JOB=%d", n);
		snprintf(new_env[1], ENV_SIZE, "THREAD=%d", th);
		execle("/bin/sh", "sh", "-c", cmd, NULL, new_env);
		perror("execle");
		exit(1);
	} else {
		fprintf(stderr, "%d: %s", (int)pid, cmd);
		njobs++;
		return pid;
	}
}

int main(int argc, char *argv[])
{
	char *buf;
	size_t len;
	int i;

	maxjobs = argc == 2 ? strtoul(argv[1], NULL, 0) : 0;
	if (!maxjobs) {
		fputs("usage: prll <number of jobs>\n", stderr);
		return -1;
	}

	jobs = malloc(maxjobs * sizeof jobs);
	for (i = 0; environ[i]; i++)
		;
	new_env = malloc((3+i) * sizeof *new_env);
	for (i = 0; environ[i]; i++)
		new_env[2+i] = environ[i];
	new_env[2+i] = NULL;
	new_env[0] = malloc(ENV_SIZE);
	new_env[1] = malloc(ENV_SIZE);
	len = 0;
	buf = NULL;
	i = 0;
	while (-1 != getline(&buf, &len, stdin)) {
		while (njobs == maxjobs)
			wait_for_child();
		jobs[free_job] = runcmd(buf, free_job, i++);
		free_job++;
	}

	if (ferror(stdin)) {
		perror("stdin");
		nerrors++;
	}
	if (buf)
		free(buf);

	while (njobs > 0)
		wait_for_child();

	if (nerrors) {
		fprintf(stderr, "%d/%d jobs failed\n", nerrors, nsucceses+nerrors);
		return 1;
	}
	return 0;
}
